create schema res_productions;
use res_productions;
create table user_registrations(
first_name text  NOT NULL,
last_name text  NOT NULL,
gender enum('M','F') NOT NULL,
dob  date NOT NULL,
user_type enum('N','P') NOT NULL,
address text NOT NULL,
phone_no int NOT  NULL,
created_on date NOT NULL,
modified_on date NOT NULL,
created_by text NOT NULL,
modified_by text NOT NULL,
book_status enum('0','1') NOT NULL
);
create table book_details(
book_title varchar(40) NOT NULL,
book_author varchar(40) NOT NULL,
book_publisher varchar(40) NOT NULL,
book_genre varchar(40) NOT NULL,
yearOfRelease year NOT null,
Rating int NOT NULL,
created_on date NOT NULL,
modified_on date NOT NULL,
created_by text NOT NULL,
modified_by text NOT NULL,
book_status enum('0','1') NOT NULL
);
create table author_details(
author_name text  NOT NULL,
author_dob  date NOT NULL,
author_description text NOT NULL,
created_on date NOT NULL,
modified_on date NOT NULL,
created_by text NOT NULL,
modified_by text NOT NULL,
book_status enum('0','1') NOT NULL
);
create table publisher_details(
publisher_name text  NOT NULL,
established_on date NOT NULL,
publisher_address text NOT NULL,
publisher_description text NOT NULL,
created_on date NOT NULL,
modified_on date NOT NULL,
created_by text NOT NULL,
modified_by text NOT NULL,
book_status enum('0','1') NOT NULL
);
create table user_featuress(
rented_date date NOT NULL,
liked_book text NOT NULL,
wishlist_books text NOT NULL,
user_friend_name text NOT NULL,
user_friend_detail text NOT NULL,
created_on date NOT NULL,
modified_on date NOT NULL,
created_by text NOT NULL,
modified_by text NOT NULL,
book_status enum('0','1') NOT NULL
);
alter table user_featuress
add rented_book text;
alter table user_featuress
add user_name_f text;

