INSERT INTO author_details(author_name, author_dob , author_description,created_on,modified_on,created_by ,modified_by ,book_status )
VALUES ('chetan bhagat', '1987-04-22','indian author','2020-06-20','2020-06-20','richa','richa','1');

INSERT INTO publisher_details ( publisher_name,established_on,publisher_address,publisher_description,created_on,modified_on,created_by ,modified_by ,book_status)
VALUES ('penguin', '1987-04-22','mumbai','indian publisher','2020-06-20','2020-06-20','richa','richa','1');

INSERT INTO publisher_details ( publisher_name,established_on,publisher_address,publisher_description,created_on,modified_on,created_by ,modified_by ,book_status)
VALUES ('ankit publisher', '1987-04-22','delhi','indian publisher','2020-06-20','2020-06-20','richa','richa','1');

INSERT INTO book_details(book_title,book_author,book_publisher,book_genre,yearOfRelease,Rating,created_on ,modified_on,created_by,modified_by,book_status)
values ('3 msitake of my life','chetan bhagat','abc','comedy','2008','4','2020-06-20','2020-06-20','richa','richa','1');

INSERT INTO user_registrations(first_name,last_name,gender,dob,user_type,address,phone_no ,created_on,modified_on,created_by,modified_by,book_status )
values ('richa','rani','F','1999-11-24','P','darbhanga,bihar','72223','2020-06-20','2020-06-20','richa','richa','1');

insert into user_featuress(rented_date,liked_book,wishlist_books,user_friend_name,user_friend_detail,created_on ,modified_on,created_by,modified_by,book_status,rented_book)
values ( '2020-03-19','3 mistake of my life','half girlfriend','richa','abc','2020-06-20','2020-04-22','richa','richa','1','alchimist');
select rented_book from user_featuress;

select liked_book from user_featuress;

select first_name from user_registrations where user_type='P';

select first_name,last_name,dob,user_type,address,phone_no from user_registrations where gender='F';

select book_genre from book_details;

select book_title from book_details where Rating>4;

select book_title ,max(Rating) as maxrating from book_details group by book_title; 

select book_title ,min(Rating) as maxrating from book_details group by book_title; 

select author_name from author_details where author_name like 'AR%';

select * from publisher_details where established_on<2012;

select book_title from book_details  where rating >4 group by book_title limit 5;

select first_name from user_registrations where user_featuress.user_friend_name;

select * from publisher_details where established_on between 2012 and 2018;

select first_name from user_registrations where user_type='p' limit 5;

select author_name from author_details group by author_name;

select liked_book from user_featuress where user_friend_name in(select book_title from book_details);

select rented_book from user_featuress where user_friend_name in(select book_title from book_details);

select wishlist_books from user_featuress where  user_friend_name  in(select first_name from user_registrations);

select user_name_f, count(rented_book) as count from user_featuress where count>10 in (select user_name from user_registrations having user_type='p');

delete  liked_book from user_featuress where user_friend_name in(select book_title from book_details);
