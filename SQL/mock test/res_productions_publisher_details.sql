-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: res_productions
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `publisher_details`
--

DROP TABLE IF EXISTS `publisher_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `publisher_details` (
  `publisher_name` text NOT NULL,
  `established_on` date NOT NULL,
  `publisher_address` text NOT NULL,
  `publisher_description` text NOT NULL,
  `created_on` date NOT NULL,
  `modified_on` date NOT NULL,
  `created_by` text NOT NULL,
  `modified_by` text NOT NULL,
  `book_status` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `publisher_details`
--

LOCK TABLES `publisher_details` WRITE;
/*!40000 ALTER TABLE `publisher_details` DISABLE KEYS */;
INSERT INTO `publisher_details` VALUES ('penguin','1987-04-22','mumbai','indian publisher','2020-06-20','2020-06-20','richa','richa','1'),('penguin','1987-04-22','mumbai','indian publisher','2020-06-20','2020-06-20','richa','richa','1'),('ankit publisher','1987-04-22','delhi','indian publisher','2020-06-20','2020-06-20','richa','richa','1'),('penguin','1987-04-22','mumbai','indian publisher','2020-06-20','2020-06-20','richa','richa','1'),('ankit publisher','1987-04-22','delhi','indian publisher','2020-06-20','2020-06-20','richa','richa','1'),('penguin','1987-04-22','mumbai','indian publisher','2020-06-20','2020-06-20','richa','richa','1'),('ankit publisher','1987-04-22','delhi','indian publisher','2020-06-20','2020-06-20','richa','richa','1'),('penguin','1987-04-22','mumbai','indian publisher','2020-06-20','2020-06-20','richa','richa','1'),('ankit publisher','1987-04-22','delhi','indian publisher','2020-06-20','2020-06-20','richa','richa','1'),('penguin','1987-04-22','mumbai','indian publisher','2020-06-20','2020-06-20','richa','richa','1'),('ankit publisher','1987-04-22','delhi','indian publisher','2020-06-20','2020-06-20','richa','richa','1'),('penguin','1987-04-22','mumbai','indian publisher','2020-06-20','2020-06-20','richa','richa','1'),('ankit publisher','1987-04-22','delhi','indian publisher','2020-06-20','2020-06-20','richa','richa','1'),('penguin','1987-04-22','mumbai','indian publisher','2020-06-20','2020-06-20','richa','richa','1'),('ankit publisher','1987-04-22','delhi','indian publisher','2020-06-20','2020-06-20','richa','richa','1'),('penguin','1987-04-22','mumbai','indian publisher','2020-06-20','2020-06-20','richa','richa','1'),('ankit publisher','1987-04-22','delhi','indian publisher','2020-06-20','2020-06-20','richa','richa','1'),('penguin','1987-04-22','mumbai','indian publisher','2020-06-20','2020-06-20','richa','richa','1'),('ankit publisher','1987-04-22','delhi','indian publisher','2020-06-20','2020-06-20','richa','richa','1');
/*!40000 ALTER TABLE `publisher_details` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-20 14:04:09
