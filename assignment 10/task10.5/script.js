var companies = {
     TCS : {
        revenue : 100000000,
        expenses : {
        salaries : 30,
        rent : 20,
        utilities : 15
        },
        employees : [
            {
            name : "a",
            age : 21,
            role : "Admin"
            },
            {
            name : "b",
            age : 25,
            role : "Tester"
            },
            {
            name : "c",
            age : 35,
            role : "Programmer"
            }
        ]

     },
     GGK:{
        revenue : 200000000,
        expenses : {
            salaries : 10,
            rent : 10,
            utilities : 20
        },
        employees : [
            {
            name : "d",
            age : 20,
            role : "Admin"
            },
            {
            name : "e",
            age : 27,
            role : "Tester"
            },
            {
            name : "f",
            age : 38,
            role : "Programmer"
            }
        ]
     },
     Osmosys : {
        revenue : 1000000000,
        expenses : {
        salaries : 20,
        rent : 8,
        utilities : 15
        },
        employees : [
            {
            name : "g",
            age : 22,
            role : "Admin"
            },
            {
            name : "h",
            age : 37,
            role : "Tester"
            },
            {
            name : "i",
            age : 31,
            role : "Programmer"
            }
        ]
     }
}

// console.log(companies);

function getAllEmployees() {
    let allEmployees =[];
    for( let x in companies) {
        companies [x].employees.forEach(element => {
            allEmployees.push(element);
        });
    }
    return allEmployees;
}

function getEmpByAges(ageLimit, isLessThan, employeesList) {

    let employees = [];

    if(isLessThan) {
        employees = employeesList.filter((employee) => employee["age"] <= ageLimit);
    }
    else {
        employees = employeesList.filter((employee) => employee["age"] > ageLimit);
    }
     
    return employees;

}

function youngestAndOldest(allEmployees){
    //Find youngest age and oldest age
    let small = 1000, large = 0;

    

    allEmployees.forEach((e) => {
        if(e.age < small){
            small = e.age;
        }
        if(e.age > large){
            large = e.age;
        }
    });

    allEmployees.forEach((e) => {
        if(e.age === small){
            console.log(`${e.name} is youngest employee.`)
        }
        if(e.age === large){
            console.log(`${e.name} is Oldest employee.`)
        }
        
    });
}


function getEmployeesOnRoleBasis(){
    const allEmployees = getAllEmployees();

    const programmers = allEmployees.filter(e => e.role === "Programmer");
    const admins = allEmployees.filter(e => e.role === "Admin");
    const Testers = allEmployees.filter(e => e.role === "Tester");

    programmers.forEach(e => {console.log(`${e.name} is programmer.`)});
    admins.forEach(e => {console.log(`${e.name} is admin.`)});
    Testers.forEach(e => {console.log(`${e.name} is Tester.`)});
}

function getRevenueOfAll(){
    let revenue =[];
    for( companieName in companies){
        revenue.push([companieName , companies[companieName].revenue]);
    }
    return revenue;
}
function getProfitOfAll(){
    let expenses =[];
    for( companieName in companies){
        let exopensePercentage =0;
        for(expense in companies[companieName].expenses){
            exopensePercentage += companies[companieName].expenses[expense]
        }
        expenses.push([companieName , (companies[companieName].revenue)*((100-exopensePercentage)/100)]);
        console.log(`${companieName} is earing prfit of rupees ${(companies[companieName].revenue)*((100-exopensePercentage)/100)}`);
    }
    return expenses;
};
function printMostEarningCompany(){
    let profitOfAll =getProfitOfAll();   
    let max =0;
    profitOfAll.forEach(x => {
        if(x[1]>max){
            max = x[1];
        }
    });    

    profitOfAll.forEach(x => {
        if(x[1]===max){
            console.log(`${x[0]} is earning most profit amoung all companies`);
        }
    }); 
};

/* ageLessThanOrEql((prompt("Enter Age for search for age less than or equal to:")));

ageGreaterThan((prompt("Enter Age for search for age greater than:"))); 

youngestAndOldest(); 

getEmployeesOnRoleBasis();*/
