const question = document.getElementById("question");
const choices = Array.from(document.getElementsByClassName("choice-text"));
const questionCounterText = document.getElementById("questionCounter");
const scoreText = document.getElementById("score");

let currentQuestion = {};
let acceptingAnswers = false;
let score = 0;
let questionCounter = 0;
let availableQuesions = [];
var circle = document.createElement('img'); circle.src = 'circle.png'; 
var triangle = document.createElement('img'); triangle.src = 'triangle.png'; 
var square = document.createElement('img'); square.src = 'square.png'; 
var rectangle = document.createElement('img'); rectangle.src = 'rectangle.png'; 
var flower = document.createElement('img'); flower.src = 'flower.jpg'; ;
var tree = document.createElement('img'); tree.src = 'tree.jpg'; 
var cloud = document.createElement('img'); cloud.src = 'cloud.jpg'; 
var crow = document.createElement('img'); crow.src = 'crow.jpg'; 
let questions = [
  {
    question: "Click on a circle shaped object",
    choice1:  document.getElementById('choice-text-1').appendChild(circle).height = "30",
    choice2:  document.getElementById('choice-text-2').appendChild(triangle).height = "30",
    choice3:  document.getElementById('choice-text-3').appendChild(tree).height = "30",
    choice4:  document.getElementById('choice-text-4').appendChild(rectangle).height = "30",
    answer: 1
  },
  {
    question: "Click on a rectangle shaped object",
    choice1: document.getElementById('choice-text-1').appendChild(triangle).height = "30",
    choice2: document.getElementById('choice-text-2').appendChild(square).height = "30",
    choice3: document.getElementById('choice-text-4').appendChild(rectangle).height = "30",
    choice4: document.getElementById('choice-text-4').appendChild(flower).height = "30",
    answer: 3
  },
  {
    question: " Click on a triangle shaped object",
    choice1:  document.getElementById('choice-text').appendChild(crow).height = "30",
    choice2: document.getElementById('choice-text').appendChild(tree).height = "30",
    choice3: document.getElementById('choice-text').appendChild(cloud).height = "30",
    choice4: document.getElementById('choice-text-1').appendChild(triangle).height = "30",
    answer: 4
  },
  {
    question: " Click on a square shaped object",
    choice1:  document.getElementById('choice-text-4').appendChild(rectangle).height = "30",
    choice2:   document.getElementById('choice-text-1').appendChild(circle).height = "30",
    choice3:  document.getElementById('choice-text-2').appendChild(triangle).height = "30",
    choice4:   document.getElementById('choice-text-2').appendChild(square).height = "30",
    answer: 4
  },
  {
    question: " Click on a cloud",
    choice1:  document.getElementById('choice-text').appendChild(tree).height = "30",
    choice2:  document.getElementById('choice-text').appendChild(crow).height = "30",
    choice3:  document.getElementById('choice-text-4').appendChild(flower).height = "30",
    choice4:  document.getElementById('choice-text').appendChild(cloud).height = "30",
    answer: 4
  },
  {
    question: " Click on a flower plant",
    choice1:  document.getElementById('choice-text').appendChild(tree).height = "30",
    choice2:    document.getElementById('choice-text').appendChild(cloud).height = "30",
    choice3:   document.getElementById('choice-text-2').appendChild(square).height = "30",
    choice4:  document.getElementById('choice-text-4').appendChild(flower).height = "30",
    answer: 4
  },
  {
    question: " Click on a tree",
    choice1:  document.getElementById('choice-text-4').appendChild(flower).height = "30",
    choice2:  document.getElementById('choice-text').appendChild(cloud).height = "30",
    choice3:  document.getElementById('choice-text').appendChild(tree).height = "30",
    choice4:  document.getElementById('choice-text').appendChild(crow).height = "30",
    answer: 3
  },
  {
    question: " Click on the crow",
    choice1:    document.getElementById('choice-text-4').appendChild(rectangle).height = "30",
    choice2:    document.getElementById('choice-text').appendChild(cloud).height = "30",
    choice3:    document.getElementById('choice-text-4').appendChild(flower).height = "30",
    choice4:    document.getElementById('choice-text').appendChild(crow).height = "30",
    answer: 4
  }
];

//CONSTANTS
const CORRECT_BONUS = 10;
const MAX_QUESTIONS = 8;

startGame = () => {
  questionCounter = 0;
  score = 0;
  availableQuesions = [...questions];
  getNewQuestion();
};

getNewQuestion = () => {
  if (availableQuesions.length === 0 || questionCounter >= MAX_QUESTIONS) {
    //go to the end page
    return window.location.assign("end.html");
  }
  questionCounter++;
  questionCounterText.innerText = `${questionCounter}/${MAX_QUESTIONS}`;

  const questionIndex = Math.floor(Math.random() * availableQuesions.length);
  currentQuestion = availableQuesions[questionIndex];
  question.innerText = currentQuestion.question;

  choices.forEach(choice => {
    const number = choice.dataset["number"];
    choice.innerText = currentQuestion["choice" + number];
  });

  availableQuesions.splice(questionIndex, 1);
  acceptingAnswers = true;
};

choices.forEach(choice => {
  choice.addEventListener("click", e => {
    if (!acceptingAnswers) return;

    acceptingAnswers = false;
    const selectedChoice = e.target;
    const selectedAnswer = selectedChoice.dataset["number"];

    const classToApply =
      selectedAnswer == currentQuestion.answer ? "correct" : "incorrect";

    if (classToApply === "correct") {
      incrementScore(CORRECT_BONUS);
    }

    selectedChoice.parentElement.classList.add(classToApply);

    setTimeout(() => {
      selectedChoice.parentElement.classList.remove(classToApply);
      getNewQuestion();
    }, 1000);
  });
});

incrementScore = num =>
{
  score += num;
  scoreText.innerText = score;
};

startGame();