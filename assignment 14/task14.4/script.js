function outerFunc() {
    var original = [
        {
            "filename": "flower",
            "type": "jpg"
        },
        {
            "filename": "family-video-clip",
            "type": "mp4"
        },
        {
            "filename": "phone-ringtone",
            "type": "mp3"
        },
        "javascript-exercise.txt",
        "learning-html-basics.rtf",
        {
            "filename": "family-video-clip",
            "type": "mp4"
        },
        {
            "filename": "resume",
            "type": "docx"
        },
        {
            "filename": "student-report",
            "type": "csv"
        },
        {
            "filename": "sms-ringtone",
            "type": "mp3"
        },
        "html-basics.pdf",
        "dubsmash.mp4",
        "screen-shot.png",
        {
            "filename": "faculty-report",
            "type": "xlsx"
        },
        {
            "filename": "puppy",
            "type": "svg"
        }
    ]
    var answer = [];
    answer.push({ "audio": [], "video": [], "document": [], "image": [] })
    var dict = {};
    dict["audio"] = new Set(["mp3"])
    dict["image"] = new Set(["jpg", "svg", "png"])
    dict["document"] = new Set(["docx", "xlsx", "csv", "pdf", "txt"])
    dict["video"] = new Set(["mp4"])


    function innerFunc() {
        for (var i = 0; i < original.length; i++) {
            curr = original[i]
            if (typeof curr == "string") {
                var idx = curr.lastIndexOf(".");
                var curr_type = curr.slice(idx + 1, curr.length);
                var curr_name = curr.slice(0, idx);
            }
            else {
                var curr_type = curr.type
                var curr_name = curr.filename
            }
            for (var [key, value] of Object.entries(dict)) {
                if (value.has(curr_type) == true) {
                    answer[0][key].push(curr_name)
                }
            }
        }
        var json_obj = JSON.stringify(answer);
        console.log(json_obj);
    }
    return innerFunc;
}

var obj = new outerFunc();
obj();