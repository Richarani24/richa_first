
const container = document.querySelector('.seat-container');
const seats = document.querySelectorAll('.row .seat:not(.occupied)');
const count = document.getElementById('count');
const price = document.getElementById('price');

const dateSelect = document.getElementById('booking');
let ticketPrice = +dateSelect.value;

const populateUI = () => {
  const selectedSeats = JSON.parse(localStorage.getItem('selectedSeats'));

  if (selectedSeats !== null && selectedSeats.length > 0) {
    seats.forEach((seat, index) => {
      if (selectedSeats.indexOf(index) > -1) {
        seat.classList.add('selected');
      }
    });
  }

  const selectedBookIndex = localStorage.getItem('selectedbookingIndex');
  const selectedBookPrice = localStorage.getItem('selectedbookingPrice');

  if (selectedBookIndex !== null) {
    dateSelect.selectedIndex = selectedBookIndex;
  }

  if (selectedBookPrice !== null) {
    count.innerText = selectedSeats.length;
    price.innerText = selectedSeats.length * +selectedBookPrice;
  }
};

populateUI();

selectedBooking = (BookIndex, BookPrice) => {
  localStorage.setItem('selectedMovieIndex', BookIndex);
  localStorage.setItem('selectedMoviePrice', BookPrice);
};
//update the seat count
const updateSelectedSeatsCount = () => {
  const selectedSeats = document.querySelectorAll('.row .selected');

  const seatsIndex = [...selectedSeats].map(seat => [...seats].indexOf(seat));

  localStorage.setItem('selectedSeats', JSON.stringify(seatsIndex));

  const selectedSeatsCount = selectedSeats.length;

  count.innerText = selectedSeatsCount;
  price.innerText = selectedSeatsCount * ticketPrice;
};

// Seat select event
container.addEventListener('click', e => {
  if (e.target.classList.contains('seat') &&!e.target.classList.contains('occupied'))
   {
    e.target.classList.toggle('selected');

    updateSelectedSeatsCount();
  }
});

// ticket select event
dateSelect.addEventListener('change', e => {
  ticketPrice = +e.target.value;
  selectedBooking(e.target.selectedIndex, e.target.value);

  updateSelectedSeatsCount();
});

