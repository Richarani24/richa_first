var click = 0;
var outOfTime = false;
function red() {
  if (!outOfTime) {
    click -= 2;
    document.getElementById("score").innerHTML = click;
  }
}
function voilet() {
  if (!outOfTime) {
    click -= 1;
    document.getElementById("score").innerHTML = click;
  }
}
function green() {
  if (!outOfTime) {
    click += 3;
    document.getElementById("score").innerHTML = click;
  }
}
function yellow() {
  if (!outOfTime) {
    click += 2;
    document.getElementById("score").innerHTML = click;
  }
}
function blue() {
  if (!outOfTime) {
    click += 1;
    document.getElementById("score").innerHTML = click;
  }
}
var time = 60; //time in seconds
var timer = setInterval(function () {
  time--;
  document.getElementById("timer").innerHTML = time + "sec";
  if (time == 0) {
    clearInterval(timer);
    document.getElementById("timer").innerHTML = "Game Over";
    outOfTime = true;
  }
}, 1000);

jQuery(document).ready(function ($) {
  var num = 100;
  for (var x = 1; x <= num; x++) 
  {
    var randomDelay = Math.random() * 10;
    $(".red")
      .css("animation-delay", randomDelay + "s")
      .appendTo("body");
  }
  // get window dimensions
  var ww = $(".container").width();
  var wh = $(".container").height();
  $(".red").each(function (i) 
  {
    var posx = Math.round(Math.random() * ww) - 20;
    var posy = Math.round(Math.random() * wh) - 20;
    $(this)
      .css("top", posy + "px")
      .css("left", posx + "px");
  });
  $(".yellow").each(function (i) 
  {
    var posx = Math.round(Math.random() * ww) - 20;
    var posy = Math.round(Math.random() * wh) - 20;
    $(this)
      .css("top", posy + "px")
      .css("left", posx + "px");
  });
  $(".blue").each(function (i) 
  {
    var posx = Math.round(Math.random() * ww) - 20;
    var posy = Math.round(Math.random() * wh) - 20;
    $(this)
      .css("top", posy + "px")
      .css("left", posx + "px");
  });
  $(".green").each(function (i) 
  {
    var posx = Math.round(Math.random() * ww) - 20;
    var posy = Math.round(Math.random() * wh) - 20;
    $(this)
      .css("top", posy + "px")
      .css("left", posx + "px");
  }); 
  $(".voilet").each(function (i)
  {
    var posx = Math.round(Math.random() * ww) - 20;
    var posy = Math.round(Math.random() * wh) - 20;
    $(this)
      .css("top", posy + "px")
      .css("left", posx + "px");
  });
});
/*$(document).ready(function(){
    $("div.red").click(function(){
      var div = $("div.red");
      div.hide().animate({left: '200px', opacity: '60'}, "fast");
      div.show().animate({right: '500px', opacity: '60'}, "fast");
      div.hide().animate({left: '100px', opacity: '60'}, "fast");
      div.show().animate({marginleft: '50px', opacity: '60'}, "fast");
    });
    $("div.voilet").click(function(){
      var div2 = $("div.voilet");
      div2.animate({margin: '20px', opacity: '60'}, "fast");
      div2.animate({marginleft: '50px', opacity: '60'}, "fast");
      div2.animate({marginleft: '200px', opacity: '60'}, "fast");
      div2.animate({marginleft: '500px', opacity: '60'}, "fast");
    });
    $("div.yellow").click(function(){
      var div3 = $("div.yellow");
      div3.animate({marginleft: '20px', opacity: '20'}, "fast");
      div3.animate({marginleft: '50px', opacity: '20'}, "fast");
      div3.animate({margin: '20px', opacity: '20'}, "fast");
      div3.animate({margin: '50px', opacity: '20'}, "fast");
    });
    $("div.blue").click(function(){
      var div4 = $("div.blue");
      div4.animate({marginleft: '30px', opacity: '20'}, "fast");
      div4.animate({marginleft: '20px', opacity: '20'}, "fast");
      div4.animate({margin: '30px', opacity: '20'}, "fast");
      div4.animate({margin: '20px', opacity: '20'}, "fast");
    });
    $("div.green").click(function(){
      var div5 = $("div.green");
      div5.animate({marginleft: '200px', opacity: '20'}, "fast");
      div5.animate({marginleft: '500px', opacity: '20'}, "fast");
      div5.animate({margin: '20px', opacity: '20'}, "fast");
      div5.animate({margin: '50px', opacity: '20'}, "fast");
    });
  });*/